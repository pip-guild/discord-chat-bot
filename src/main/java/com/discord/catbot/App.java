package com.discord.catbot;

import com.discord.catbot.listeners.CommandListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;

public class App
{
    public static void main( String[] args )
    {
        String token = System.getenv("TOKEN");

        JDA builder = null;
        try {
            builder = JDABuilder.createDefault(token).enableIntents( GatewayIntent.GUILD_MESSAGE_TYPING, GatewayIntent.DIRECT_MESSAGES,GatewayIntent.MESSAGE_CONTENT)
                    .setActivity(Activity.playing("Minecraft"))
                    .build();
            builder.addEventListener(new CommandListener());
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }
}
