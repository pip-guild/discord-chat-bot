package com.discord.catbot.listeners;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandListener extends ListenerAdapter {
    private final List<Integer> codes = new ArrayList<>(Arrays.asList(100,101,102,200,201,202,203,204,206,207,
                300,301,302,303,304,305,307,308,400,401,402,403,404,405,406,407,408,409,410,
                411,412,413,414,415,416,417,418,420,421,422,423,424,425,426,429,431,444,450,451,
                497,498,499,500,501,502,503,504,506,507,508,509,510,511,521,522,523,525,599));

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        Message msg = event.getMessage();
        MessageChannel channel = msg.getChannel();
        if((msg.getContentRaw().contains("code")) &&
                !(msg.getContentRaw().indexOf("code") < 4)){
            int lstIndx = msg.getContentRaw().indexOf("code");
            String num = msg.getContentRaw().substring(lstIndx - 4, lstIndx).trim();
            if(CommandListener.isNumeric(num)){
                if(codes.contains(Integer.valueOf(num))){
                    channel.sendMessage("https://http.cat/" + num + ".jpg").queue();
                }
            }
        }
    }

    private static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
}
